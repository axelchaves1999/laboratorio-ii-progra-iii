﻿using Mantenimiento_Platillos.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Windows.Forms;

namespace Mantenimiento_Platillos.DAO
{
    class XMLDAO
    {
        private XmlDocument doc;
        private string ruta = "";

        public XMLDAO() {
            CargarRuta();
        }
        private void CargarRuta()
        {
            DirectoryInfo d = new DirectoryInfo(@"..\\..\\XML");
            FileInfo[] info = d.GetFiles("*.xml");
            foreach (FileInfo f in info)
            {
                ruta += f.FullName;
            }
        }
        public bool Insertar(XML xml)
        {
            try
            {
                doc = new XmlDocument();
                XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", "no");
                XmlNode nodo = doc.DocumentElement;
                doc.InsertBefore(xmlDeclaration, nodo);
                XmlNode basedatos = doc.CreateElement(xml.BaseDatos);
                doc.AppendChild(basedatos);
                BorrarDirectorios();
                GuadarXML(xml);
                doc.Save(ruta);
                return true;
            }
            catch (Exception)
            {
                
                MessageBox.Show("Favor intentar nuevamente");

            }
            return false;

        }

        private void GuadarXML(XML xml)
        {
            StreamWriter writter = new StreamWriter(@"..\\..\\Archivos\\XML.text");
            string txt = string.Format("{0},{1}",xml.BaseDatos,xml.Tabla) ;
            writter.WriteLine(txt);
            writter.Close();
        }

        private void BorrarDirectorios()
        {
            DirectoryInfo d = new DirectoryInfo(@"..\\..\\XML");
            FileInfo[] info = d.GetFiles("*.xml");
           
            foreach (FileInfo f in info)
            {
                f.Delete();
            }
        }


    }
}
