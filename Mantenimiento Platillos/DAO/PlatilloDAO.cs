﻿using Mantenimiento_Platillos.Entidades;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Mantenimiento_Platillos.DAO
{
    class PlatilloDAO
    {
        XmlDocument doc;
        private string ruta;



        public PlatilloDAO()
        {
            CargarRuta();
        }

        public bool Validar(Platillo p)
        {
            doc = new XmlDocument();
            doc.Load(ruta);
            XmlNodeList platillos = doc.SelectNodes(CargarTabla()[0] + "/" + CargarTabla()[1]);
            foreach (XmlNode plat in platillos)
            {
                if (plat.SelectSingleNode("Nombre").InnerText.Equals(p.Nombre))
                {
                    return false;
                }
            }
            return true;
        }

        public IEnumerable<Platillo> Seleccionar(string filtro)
        {
            doc = new XmlDocument();
            doc.Load(ruta);
            List<Platillo> listaPlat = new List<Platillo>();
            XmlNodeList platillos = doc.SelectNodes(PrepararRuta());

            foreach (XmlNode p in platillos)
            {
                Platillo plat = new Platillo();
                plat.Id = Int32.Parse(p.SelectSingleNode("Id").InnerText);
                plat.Nombre = p.SelectSingleNode("Nombre").InnerText;
                plat.Precio = p.SelectSingleNode("Precio").InnerText;
                plat.Descripcion = p.SelectSingleNode("Descripcion").InnerText;
                plat.Calorias = p.SelectSingleNode("Calorias").InnerText;
                if (filtro.Equals("Calorias"))
                {
                    if (Int32.Parse(plat.Calorias) >= 400 && Int32.Parse(plat.Calorias) < 700)
                    {
                        listaPlat.Add(plat);
                    }
                }
                else if (filtro.Equals("Precio")) {
                    listaPlat.Add(plat);
                }



            }
            if (filtro.Equals("Calorias"))
            {
                return ListaPreciosBajos(listaPlat);

            }
            else
            {
                return ListaCalorias(listaPlat);

            }
        }

        private IEnumerable<Platillo> ListaCalorias(List<Platillo> listaPlat)
        {
            List<Platillo> lista = listaPlat.OrderByDescending(platillo => platillo.Calorias).ToList();
            IEnumerable<Platillo> calorias = lista;

            return lista;
        }

        private IEnumerable<Platillo> ListaPreciosBajos(List<Platillo> listaPlat)
        {
            List<Platillo> lista = listaPlat.OrderByDescending(platillo => platillo.Calorias).ToList();
            IEnumerable<Platillo> calorias = lista.Skip(lista.Count - 2);
            return calorias;
        }

        public List<Platillo> Cargar()
        {
            doc = new XmlDocument();
            doc.Load(ruta);
            List<Platillo> listaPlat = new List<Platillo>();

            XmlNodeList platillos = doc.SelectNodes(PrepararRuta());


            foreach (XmlNode p in platillos)
            {
                Platillo plat = new Platillo();
                plat.Id = Int32.Parse(p.SelectSingleNode("Id").InnerText);
                plat.Nombre = p.SelectSingleNode("Nombre").InnerText;
                plat.Precio = p.SelectSingleNode("Precio").InnerText;
                plat.Descripcion = p.SelectSingleNode("Descripcion").InnerText;
                plat.Calorias = p.SelectSingleNode("Calorias").InnerText;
                listaPlat.Add(plat);
            }
            return listaPlat;
        }

        public string PrepararRuta()
        {

            return CargarTabla()[0] + "/" + CargarTabla()[1];
        }

        private void CargarRuta()
        {
            DirectoryInfo d = new DirectoryInfo(@"..\\..\\XML");
            FileInfo[] info = d.GetFiles("*.xml");
            ruta = "";
            foreach (FileInfo f in info)
            {
                ruta += f.FullName;
            }
        }
        public bool Insertar(Platillo p)
        {
            doc = new XmlDocument();
            doc.Load(ruta);
            XmlNode platillo = CrearPlatillo(p);
            XmlNode nodo = doc.DocumentElement;
            nodo.InsertBefore(platillo, nodo.LastChild);
            doc.Save(ruta);
            return true;
        }

        public XmlNode CrearPlatillo(Platillo p)
        {

            XmlNode nodo = doc.CreateElement(CargarTabla()[1]);

            XmlNode id = doc.CreateElement("Id");
            id.InnerText = Convert.ToString(p.Id);
            nodo.AppendChild(id);

            XmlNode nom = doc.CreateElement("Nombre");
            nom.InnerText = p.Nombre;
            nodo.AppendChild(nom);

            XmlNode prec = doc.CreateElement("Precio");
            prec.InnerText = p.Precio;
            nodo.AppendChild(prec);

            XmlNode des = doc.CreateElement("Descripcion");
            des.InnerText = p.Descripcion;
            nodo.AppendChild(des);

            XmlNode cal = doc.CreateElement("Calorias");
            cal.InnerText = p.Calorias;
            nodo.AppendChild(cal);
            return nodo;
        }

        public string[] CargarTabla()
        {
            StreamReader reader = new StreamReader(@"..\\..\\Archivos\\XML.text");
            string txt = "";
            if (reader != null)
            {
                txt = reader.ReadLine();
            }
            String[] atributos = txt.Split(',');
            reader.Close();
            return atributos;
        }
    }
}
