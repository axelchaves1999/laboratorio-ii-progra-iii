﻿using Mantenimiento_Platillos.DAO;
using Mantenimiento_Platillos.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimiento_Platillos.BO
{
    class XMLBO
    {
        public bool Insertar(XML xml)
        {
            return new XMLDAO().Insertar(xml);
        }
    }
}
