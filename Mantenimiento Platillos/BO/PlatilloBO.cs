﻿using Mantenimiento_Platillos.DAO;
using Mantenimiento_Platillos.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimiento_Platillos.BO
{
    class PlatilloBO
    {
        public bool Insertar(Platillo p)
        {
            if (new PlatilloDAO().Validar(p))
            {
                return new PlatilloDAO().Insertar(p);

            }
            return false;
        }

        public List<Platillo> Cargar()
        {
            return new PlatilloDAO().Cargar();
        }

        public IEnumerable<Platillo> Seleccionar(string filtro)
        {
            return new PlatilloDAO().Seleccionar(filtro);
        }
    }
}
