﻿using Mantenimiento_Platillos.BO;
using Mantenimiento_Platillos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Mantenimiento_Platillos.GUI
{
    public partial class FrmReporte : Form
    {
        private Form parent;
        public FrmReporte(Form parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        private void FrmReportecs_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cbFiltro.SelectedItem.ToString().Equals("Calorias"))
            {
                string filtro = cbFiltro.Text.Trim();
                IEnumerable<Platillo> platillos = new PlatilloBO().Seleccionar(filtro);
                chart1.Series.Add("Platillos");

                foreach (Platillo r in platillos)
                {

                    Series serie = chart1.Series.Add(r.Nombre);
                    serie.Label += r.Nombre + "\nPrecio";
                    serie.Points.Add(Int32.Parse(r.Precio));

                }
            }
            else if(cbFiltro.SelectedItem.ToString().Equals("Precio"))
            {
                string filtro = cbFiltro.Text.Trim();
                IEnumerable<Platillo> platillos = new PlatilloBO().Seleccionar(filtro);
                chart1.Series.Clear();
                chart1.Series.Add("Platillos");

                foreach (Platillo r in platillos)
                {

                    Series serie = chart1.Series.Add(r.Nombre);
                    serie.Label += r.Nombre;
                    serie.Points.Add(Int32.Parse(r.Precio));

                }
            }
           
        }

        private void FrmReporte_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parent != null) {
                parent.Show();
            }
        }
    }
}
