﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Mantenimiento_Platillos.Entidades;
using Mantenimiento_Platillos.BO;
using Mantenimiento_Platillos.DAO;
using Mantenimiento_Platillos.GUI;

namespace Mantenimiento_Platillos
{
    public partial class FrmPrincipal : Form
    {
        private int serial;
        private string rutaSerial = @"..\\..\\Archivos\\Platillos id.txt";
        public FrmPrincipal()
        {
            InitializeComponent();
            CargarSerial();
            CargarData();
        }

        private void CargarData()
        {
            dataPla.Rows.Clear();
            foreach (Platillo p in new PlatilloBO().Cargar())
            {
                dataPla.Rows.Add(p, p.Nombre, p.Precio, p.Descripcion, p.Calorias);
            }

        }

        private void CargarSerial()
        {
            StreamReader reader = new StreamReader(rutaSerial);
            string linea;
            try
            {
                linea = reader.ReadLine();
                if (linea != null)
                {
                    serial = Int32.Parse(linea);
                }
                reader.Close();

            }
            catch (Exception)
            {

                MessageBox.Show("Error al cargar los seriales");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nom = txtNom.Text.Trim();
            string prec = txtPrec.Text.Trim();
            string des = txtDes.Text.Trim();
            string cal = txtCal.Text.Trim();
            Platillo p = new Platillo { Nombre = nom, Precio = prec, Calorias = cal, Descripcion = des, Id = serial };
            if (new PlatilloBO().Insertar(p))
            {
                CargarData();
                Limpiar();
                serial++;

                MessageBox.Show("Registro realizado con éxito");
            }
            else
            {
                MessageBox.Show("Favor intentar nuevamente");
            }

        }
        private void Limpiar()
        {
            txtNom.Clear();
            txtPrec.Clear();
            txtDes.Clear();
            txtCal.Clear();

        }

        private void crearBaseDeDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRegistroXML frm = new FrmRegistroXML(this);
            this.Hide();
            frm.Show();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void reportesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmReporte frm = new FrmReporte(this);
            this.Hide();
            frm.Show();
        }

        private void FrmPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            GuardarId();
        }

        private void GuardarId()
        {
            StreamWriter writter = new StreamWriter(rutaSerial);
            writter.WriteLine(serial);
            writter.Close();
        }
    }
}
