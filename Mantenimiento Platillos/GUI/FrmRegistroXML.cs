﻿using Mantenimiento_Platillos.BO;
using Mantenimiento_Platillos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mantenimiento_Platillos.DAO
{
    public partial class FrmRegistroXML : Form
    {
        private Form parent;
        public FrmRegistroXML(Form parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string baseDatos = txtNomBase.Text;
            string nomTabl = txtNomTabla.Text;
            XML xml = new XML { BaseDatos = baseDatos, Tabla = nomTabl };
            if (new XMLBO().Insertar(xml)) {
                MessageBox.Show("Registro realizado con éxito");
            }
        }

        private void FrmRegistroXML_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parent != null) {
                parent.Show();
            }
        }
    }
}
